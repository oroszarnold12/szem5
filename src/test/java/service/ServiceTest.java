package service;

import domain.Grade;
import domain.Homework;
import domain.Student;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

class ServiceTest {
    public static Service service;

    @org.junit.jupiter.api.BeforeAll
    public static void setUp() {
        Validator<Student> studentValidator = new StudentValidator();
        Validator<Homework> homeworkValidator = new HomeworkValidator();
        Validator<Grade> gradeValidator = new GradeValidator();

        StudentXMLRepository fileRepository1 = new StudentXMLRepository(studentValidator, "students.xml");
        HomeworkXMLRepository fileRepository2 = new HomeworkXMLRepository(homeworkValidator, "homework.xml");
        GradeXMLRepository fileRepository3 = new GradeXMLRepository(gradeValidator, "grades.xml");

        service = new Service(fileRepository1, fileRepository2, fileRepository3);
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void findAllStudents() {
        Iterable<Student> students = service.findAllStudents();
        int counter = 0;
        for (Object ignored : students) {
            counter++;
        }

        service.saveStudent("12", "Nagy Abel", 234);
        Iterable<Student> result = service.findAllStudents();
        int resultCounter = 0;
        for (Object ignored : result) {
            resultCounter++;
        }
        assertEquals(counter + 1, resultCounter);
        service.deleteStudent("12");
    }

    @org.junit.jupiter.api.Test
    void findAllHomework() {
        Iterable<Homework> homeworks = service.findAllHomework();
        int counter = 0;
        for (Object ignored : homeworks) {
            counter++;
        }

        Homework homework = new Homework("47", "WSS homework", 6, 2);
        service.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(), homework.getStartline());
        Iterable<Homework> result = service.findAllHomework();
        int resultCounter = 0;
        for (Object ignored : result) {
            resultCounter++;
        }
        assertEquals(counter + 1, resultCounter);
        service.deleteStudent("12");
    }

    @org.junit.jupiter.api.Test
    void findAllGrades() {
    }

    @org.junit.jupiter.api.Test
    void saveStudent() {
        int result = service.saveStudent("12", "Nagy Abel", 234);
        assertEquals(1, result);
        service.deleteStudent("12");
    }

    @org.junit.jupiter.api.Test
    void saveHomework() {
        Homework homework = new Homework("47", "WSS homework", 6, 2);
        int result = service.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(), homework.getStartline());
        assertEquals(1, result);
        service.deleteHomework(homework.getID());
    }

    @org.junit.jupiter.api.Test
    void saveGrade() {
    }

    @org.junit.jupiter.api.Test
    void deleteStudent() {
        service.saveStudent("12", "Nagy Abel", 234);
        int result = service.deleteStudent("12");
        assertEquals(1, result);
    }

    @org.junit.jupiter.api.Test
    void deleteHomework() {
        Homework homework = new Homework("47", "WSS homework", 6, 2);
        service.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(), homework.getStartline());
        int result = service.deleteHomework(homework.getID());
        assertEquals(1, result);
    }

    @org.junit.jupiter.api.Test
    void updateStudent() {
        service.saveStudent("12", "Nagy Abel", 234);
        Student newStudent = new Student("12", "Kis Abel", 222);
        service.updateStudent(newStudent.getID(), newStudent.getName(), newStudent.getGroup());
        Iterable<Student> students = service.findAllStudents();
        for (Student student : students) {
            if (student.getID().equals(newStudent.getID())) {
                assertAll(
                        () -> assertEquals(student.getName(), newStudent.getName())
                );
            }
        }
    }

    @org.junit.jupiter.api.Test
    void updateHomework() {
        service.saveHomework("47", "WSS homework", 6, 2);
        Homework newHomework = new Homework("47", "WSS homework", 7, 3);
        service.updateHomework(newHomework.getID(), newHomework.getDescription(), newHomework.getDeadline(), newHomework.getStartline());
        Iterable<Homework> homeworks = service.findAllHomework();
        for (Homework homework1 : homeworks) {
            if (homework1.getID().equals(newHomework.getID())) {
                assertAll(
                        () -> assertEquals(homework1.getDescription(), newHomework.getDescription()),
                        () -> assertEquals(homework1.getDeadline(), newHomework.getDeadline()),
                        () -> assertEquals(homework1.getStartline(), newHomework.getStartline())
                );
            }
        }

        service.deleteHomework(newHomework.getID());
    }

    @org.junit.jupiter.api.Test
    void updateHomeworkIncorrect() {
        service.saveHomework("47", "WSS homework", 6, 2);
        Homework newHomework = new Homework("47", "WSS homework", 15, 3);

        service.updateHomework(newHomework.getID(), newHomework.getDescription(), newHomework.getDeadline(), newHomework.getStartline());
    }

    @org.junit.jupiter.api.Test
    void extendDeadline() {
    }

    @org.junit.jupiter.api.Test
    void createStudentFile() {
    }

    @ParameterizedTest
    @ValueSource(ints = {-100, 100, 533})
    void testGroupForStudent(int group) {
        assumeTrue(group >= 0);
        Student student = new Student("12", "Mary", group);
        int result = service.saveStudent(student.getID(), student.getName(), student.getGroup());
        assertEquals(0 ,result);
        service.deleteStudent(student.getID());
    }

    @Test
    void multipleAssertions() {
        Student student = new Student("12", "Mary", 100);
        assertAll("student add",
                () -> assertEquals("12", student.getID()),
                () -> assertEquals("Mary", student.getName())

        );
    }
}
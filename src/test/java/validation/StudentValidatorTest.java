package validation;

import domain.Student;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentValidatorTest {
    public static StudentValidator studentValidator;

    @BeforeAll
    public static void setUp() {
        studentValidator = new StudentValidator();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void validate() {
        Exception exception = assertThrows(ValidationException.class, () -> {
            studentValidator.validate(new Student("", "alma", 542));
        });

        String expectedMessage = "ID invalid! \n";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.equals(expectedMessage));
    }
}